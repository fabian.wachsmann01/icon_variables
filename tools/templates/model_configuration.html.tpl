<!DOCTYPE html>
<html>
<head>
<title>model configuration {{ runid }}</title>
<style>

:root {
    --c1: #8ecae6;
    --c2: #219ebc;
    --c3: #023047;
    --c4: #ffb703;
    --c5: #fb8500;
}

div.flex {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
}

div.flex div {
    flex: 1;
}

.highlight {
    background-color: var(--c2);
}

#output_res_table td:hover {
    background-color: var(--c4);
}

#output_res_table ul {
    margin: 0;
    padding: 5px;
}

#output_res_table li {
    list-style: none;
}

ul#varlist {
    display: flex;
    flex-wrap: wrap;
}

ul#varlist li {
    list-style: none;
    border-radius: 5px;
    padding: 2px 5px;
    margin: 1px 3px;
}
ul#varlist li.show {
    background-color: var(--c4);
}
</style>
</head>
<body>
<h1>output</h1>
<div class="flex">
<div>
<h2>output resolution grid</h2>
<table id="output_res_table">
<thead>
</thead>
    <tr>
    <th></th>
    {% for r1 in resolutions[1]["values"] %}
        <th>{{ r1 }}</th>
    {% endfor %}
    </tr>
    {% for r0 in resolutions[0]["values"] %}
        <tr>
        <th>{{ r0 }}</th>
        {% for r1 in resolutions[1]["values"] %}
            <td data-categories="{% for c, s in output_categories.items() %}{% if (r0, r1) in s["active"] %} {{ c }}{% endif %}{% endfor %}"><ul>
            {% for c, s in output_categories.items() %}
                {% if (r0, r1) in s["active"] %}
                <li>{{ c }}</li>
                {% endif %}
            {% endfor %}
            </ul></td>
        {% endfor %}
        </tr>
    {% endfor %}
<tbody>
</tbody>
</table>
</div>
<div>
<h2>variables</h2>
<ul id="varlist">
{% for variable, categories in variables.items() %}
<li data-categories="{% for c in categories %} {{ c }}{% endfor %}">
{{ variable }}
</li>
{% endfor %}
</ul>
</div>
</div>
<script>
    function intersect(a, b) {
        var t;
        if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
        return a.filter(function (e) {
            return b.indexOf(e) > -1;
        });
    }

    function enter_table(element, event) {
        const thisCategories = element.getAttribute("data-categories").trim().split(" ");
        for (const thatElement of document.getElementById("varlist").getElementsByTagName("li")) {
            const thatCategories = thatElement.getAttribute("data-categories").trim().split(" ");
            const intersection = intersect(thisCategories, thatCategories);
            if (intersection.length > 0) {
                thatElement.classList.add("show");
            } else {
                thatElement.classList.add("hide");
            }
        }
    }
    function leave_table(element, event) {
        for (const thatElement of document.getElementById("varlist").getElementsByTagName("li")) {
            thatElement.classList.remove("show");
            thatElement.classList.remove("hide");
        }
    }

    function enter_list(element, event) {
        element.classList.add("highlight");
        console.log(element);

        const thisCategories = element.getAttribute("data-categories").trim().split(" ");
        for (const thatElement of document.getElementById("output_res_table").getElementsByTagName("td")) {
            const thatCategories = thatElement.getAttribute("data-categories").trim().split(" ");
            const intersection = intersect(thisCategories, thatCategories);
            if (intersection.length > 0) {
                thatElement.classList.add("highlight");
            }
        }
    }
    function leave_list(element, event) {
        element.classList.remove("highlight");
        for (const thatElement of document.getElementById("output_res_table").getElementsByTagName("td")) {
            thatElement.classList.remove("highlight");
        }
    }
    window.onload = () => {
        for (const cell of document.getElementById("output_res_table").getElementsByTagName("td")) {
            cell.addEventListener("mouseover", event => enter_table(cell, event));
            cell.addEventListener("mouseleave", event => leave_table(cell, event));
        }

        for (const cell of document.getElementById("varlist").getElementsByTagName("li")) {
            cell.addEventListener("mouseover", event => enter_list(cell, event));
            cell.addEventListener("mouseleave", event => leave_list(cell, event));
        }
    };
</script>
</body>
</html>
