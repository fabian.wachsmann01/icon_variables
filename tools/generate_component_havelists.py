import yaml
from pathlib import Path
from collections import defaultdict

vgrid_sizes = {
    "reference": "%{atmo.levels}",
    "reference_half": "%{atmo.halflevels}",
    "depth_below_sea": "%{ocean.levels}",
    "depth_below_sea_half": "%{ocean.halflevels}",
    "surface": 1,
    "meansea": 1,
    "height_2m": 1,
    "height_10m": 1,
    "atmosphere": 1,
    "toa": 1,
    "generic_ice": 1,
    "soil_depth_water": "%{soil_depth_water_levels}",
    "soil_depth_energy": "%{soil_depth_energy_levels}",
}

component_renames = {
    "jsbach": "atmo",
}

def format_haveitem(name, options):
    options = {k: v for k, v in options.items() if not (k == "size" and v == 1)}
    if len(options) == 0:
        return name
    else:
        return {name: options}

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--parsable_yaml", action="store_true", default=False, help="generate parsable yaml instead of template")
    args = parser.parse_args()

    with open(Path(__file__).parent.parent / "variables.yaml") as varfile:
        vardata = yaml.load(varfile, Loader=yaml.SafeLoader)

    vars_by_component = defaultdict(dict)
    for varname, varinfo in vardata["variables"].items():
        if "component" in varinfo and varinfo.get("vgrid", None) is not None:
            component = component_renames.get(varinfo["component"], varinfo["component"])
            vars_by_component[component][varname] = varinfo

    coupling = {"components": {
        componentname: {
            "have": [format_haveitem(varname, {"size": vgrid_sizes[varinfo["vgrid"]]})
                     for varname, varinfo in variables.items()]
        }
        for componentname, variables in vars_by_component.items()
    }}
    coupling_yaml = yaml.dump(coupling)
    if not args.parsable_yaml:
        for v in vgrid_sizes.values():
            if not isinstance(v, str):
                continue
            coupling_yaml = coupling_yaml.replace(f"'{v}'", v)
    print(coupling_yaml)

if __name__ == "__main__":
    exit(main())
