from itertools import product
import re
from pathlib import Path

import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape

basedir = Path(__file__).parent.absolute()
templatedir = basedir / "templates"

env = Environment(
        loader=FileSystemLoader(templatedir),
        autoescape=select_autoescape(),
)

def listify(e):
    if isinstance(e, list):
        return e
    else:
        return [e]


def expand_member_definition(avail, definition):
    active = set()
    for d in listify(definition):
        if d.startswith("-"):
            negate = True
            d = d[1:]
        else:
            negate = False
        pat = re.compile("^" + d.replace("*", ".*") + "$")
        for member in avail:
            if pat.match(",".join(map(str, member))):
                if negate:
                    active.remove(member)
                else:
                    active.add(member)
    return active


def include_extend(categories, category):
    if not "extend" in category:
        return category
    return {**{k: v for e in reversed(listify(category["extend"]))
               for k, v in include_extend(categories, categories[e]).items()
               if not k.endswith("_direct")},
            **category}


def _include_extensions(categories, catnames):
    for name in catnames:
        if "extend" in categories[name]:
            yield from include_extensions(categories, listify(categories[name]["extend"]))
        yield name


def include_extensions(categories, catnames):
    return list(sorted(_include_extensions(categories, catnames)))


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("config")
    parser.add_argument("outfile")
    args = parser.parse_args()

    config = yaml.load(open(args.config), Loader=yaml.SafeLoader)

    output_groups = set(product(*[d["values"] for d in config["resolutions"]]))

    output_categories = {}
    for category, settings in config["output_categories"].items():
        if "active" in settings:
            is_member_of = expand_member_definition(output_groups, settings["active"])
            output_categories[category] = {**settings, "active": is_member_of, "active_direct": is_member_of}
        else:
            output_categories[category] = settings

    output_categories = {k: include_extend(output_categories, v) for k, v in output_categories.items()}

    variables = {k: include_extensions(output_categories, listify(v)) for k, v in sorted(config["variables"].items(), key=lambda x: x[0].lower())}

    template = env.get_template("model_configuration.html.tpl")
    with open(args.outfile, "w") as outfile:
        outfile.write(template.render(runid="testrun",
                                      resolutions=config["resolutions"],
                                      output_categories=output_categories,
                                      variables=variables))

if __name__ == "__main__":
    exit(main())
